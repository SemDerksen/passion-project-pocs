// JSON.parse(localStorage.getItem(characterData))
const table = document.getElementById("characterTable");
let characterData = "";

characterDataLocal = JSON.parse(localStorage.getItem(characterData));

console.log(characterDataLocal);

generateCharacterTable();

function generateCharacterTable() {
    for (let i = 0; i < characterDataLocal.length; ++i) {
        let characterObject = characterDataLocal[i];
    
        const table = document.getElementById("character_list");
    
        let objectValues = Object.values(characterObject);
        let objectValuesFiltered = [objectValues[0], objectValues[1], objectValues[2], objectValues[5]];
    
        let row = document.createElement('tr');
    
        Object.values(objectValuesFiltered).forEach(text => {
            let cell = document.createElement('td');
            cell.appendChild(document.createTextNode(text));
            row.appendChild(cell);
        })
        table.appendChild(row);

        // row.appendChild(document.createTextNode(row.rowIndex));

        console.log("Loop: " + (1 + row.rowIndex));
        console.log(objectValues);
        console.log(objectValuesFiltered)
        // console.log(Object.values(characterObject));

        let button = document.createElement("button");
        button.innerHTML = "View";
        button.value = row.rowIndex;
        // button.onclick = fillCharacterInfo;
        row.appendChild(button);

        // console.log(document.getElementById("name"));

        button.addEventListener('click', event => {
            let selectedCharacterObject = Object.values(characterDataLocal[button.value]);

            document.getElementById("name").value = selectedCharacterObject[0];
            document.getElementById("race").value = selectedCharacterObject[1];
            document.getElementById("subrace").value = selectedCharacterObject[2];
            document.getElementById("gender").value = selectedCharacterObject[3];
            document.getElementById("age").value = selectedCharacterObject[4];
            document.getElementById("region").value = selectedCharacterObject[5];
        
            // console.log(selectedCharacterObject[0]);
        });

        let exportButton = document.createElement("button");
        exportButton.innerHTML = "Export";
        exportButton.value = row.rowIndex;
        row.appendChild(exportButton);

        exportButton.addEventListener('click', event => {
            let selectedCharacterObject = characterDataLocal[button.value];
            let selectedCharacterObjectValues = Object.values(characterDataLocal[button.value]);

            let dataStr = JSON.stringify(selectedCharacterObject);
            let dataUri = 'data:application/json;charset=utf-8,'+ encodeURIComponent(dataStr);
        
            let exportFileDefaultName = selectedCharacterObjectValues[0] + ".json";
        
            let linkElement = document.createElement('a');
            linkElement.setAttribute('href', dataUri);
            linkElement.setAttribute('download', exportFileDefaultName);
            linkElement.click();

            // SOURCE:
            // https://www.codevoila.com/post/30/export-json-data-to-downloadable-file-using-javascript
        });
    }
}

function generateCharacterList() {
    for (let i = 0; i < characterDataLocal.length; ++i) {
        let characterObject = characterDataLocal[i];
    
        const ul = document.getElementById("character_list");
        const li = document.createElement("li");
    
        let objectValues = Object.values(characterObject);
        let objectValuesFiltered = [objectValues[0], objectValues[1], objectValues[5]];
    
        li.appendChild(document.createTextNode(objectValuesFiltered));
        ul.appendChild(li);
        
        console.log(objectValues);
        console.log(objectValuesFiltered)
        // console.log(Object.values(characterObject));
    }
}

// SOURCES:
// https://www.codegrepper.com/code-examples/javascript/how+to+append+li+to+ul+in+javascript
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/values
// https://www.fwait.com/how-to-create-table-from-an-array-of-objects-in-javascript/