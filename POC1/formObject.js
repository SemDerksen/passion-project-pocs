// Forming an object with entries from a form

const objectButton = document.getElementById("object_button");
const characterForm = document.getElementById("character_form");

objectButton.addEventListener('click', event => {
    const characterFormData = new FormData(characterForm);
    const characterFormDataObject = Object.fromEntries(characterFormData.entries());

    console.log({characterFormDataObject});
});

// SOURCE:
// https://melvingeorge.me/blog/convert-html-form-tag-input-values-to-object-javascript
// https://developer.mozilla.org/en-US/docs/Learn/Forms/Your_first_form
