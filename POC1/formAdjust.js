// This adjusts the options in a selection box based on what was chosen in a previous selection box

const characterSubrace = document.getElementById("subrace");

characterRace.addEventListener('click', event => {
    const raceValue = characterRace.options[characterRace.selectedIndex].text;
    const subraceItem = document.getElementById("subrace_item");

    if (raceValue === "Azaer") {
        characterSubrace.innerHTML = "<option value=''>Select your subrace</option><option value='Human'>Human</option><option value='Nudar'>Nudar</option>";
        subraceItem.style.display = "block";
    }

    else {
        subraceItem.style.display = "none";
        characterSubrace.innerHTML = "<option value=''></option>";
    }
});

// SOURCE:
// https://stackoverflow.com/questions/28263051/change-form-select-option-when-click-and-select-another-option
