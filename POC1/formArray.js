const arrayButton = document.getElementById("array_button");
const arrayClearButton = document.getElementById("array_button");
const clearArrayButton = document.getElementById("clear_array_button");

characterArray = [];

arrayButton.addEventListener('click', event => {
    const characterFormData = new FormData(characterForm);
    const characterFormDataObject = Object.fromEntries(characterFormData.entries());
    characterArray.push(characterFormDataObject);

    console.log(characterArray);
});

clearArrayButton.addEventListener('click', event => {
    characterArray.length = 0;

    console.log(characterArray);
});

// SOURCE:
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array