// Saving an array to local Storage, as well as converting it to a string and converting it back

const saveLocalStorageButton = document.getElementById("array_localstorage_button");
const loadLocalStorageButton = document.getElementById("load_localstorage_button");
const clearLocalStorageButton = document.getElementById("clear_localstorage_button");

let characterData = "";

saveLocalStorageButton.addEventListener('click', event => {
    localStorage.setItem(characterData, JSON.stringify(characterArray));

    console.log(JSON.parse(localStorage.getItem(characterData)));
});

loadLocalStorageButton.addEventListener('click', event => {
    characterData = JSON.parse(localStorage.getItem(characterData));

    console.log(characterData);
});

clearLocalStorageButton.addEventListener('click', event => {
    localStorage.removeItem(characterData);

    console.log(JSON.parse(localStorage.getItem(characterData)));
});

// SOURCE:
// https://developer.mozilla.org/en-US/docs/Web/API/Window/localStorage
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON/stringify
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON/parse


